$(document).ready(function(){
  //register links and flowers for clicks
  //
  //link-home
  $("#link-home").click(function(){
    getdata("./home.html");
  });
  //link-about
  $("#link-about").click(function(){
    getdata("./about.html");
  });
  //link-proof
  $("#link-proof").click(function(){
    getdata("./proof.html");
  });
  //link-contact
  $("#link-contact").click(function(){
    getdata("./contact.html");
  });
  //home-flower
  $("#flower-home").click(function(){
    getdata("./home.html");
  });
  //about-flower
  $("#flower-about").click(function(){
    getdata("./about.html");
  });
  //proof-flower
  $("#flower-proof").click(function(){
    getdata("./proof.html");
  });
  //contact-flower
  $("#flower-contact").click(function(){
    getdata("./contact.html");
  });
    
  //register links and flowers for glow effect
  //
  //home
  document.getElementById("link-home").onmouseover = function(){
    document.getElementById("link-home").className = "link-glow";
    document.getElementById("flower-home").className = "flower-glow";
  };
  document.getElementById("flower-home").onmouseover = function(){
    document.getElementById("link-home").className = "link-glow";
    document.getElementById("flower-home").className = "flower-glow";
  };
  //home
  document.getElementById("link-about").onmouseover = function(){
    document.getElementById("link-about").className = "link-glow";
    document.getElementById("flower-about").className = "flower-glow";
  };
  document.getElementById("flower-about").onmouseover = function(){
    document.getElementById("link-about").className = "link-glow";
    document.getElementById("flower-about").className = "flower-glow";
  };
  //home
  document.getElementById("link-proof").onmouseover = function(){
    document.getElementById("link-proof").className = "link-glow";
    document.getElementById("flower-proof").className = "flower-glow";
  };
  document.getElementById("flower-proof").onmouseover = function(){
    document.getElementById("link-proof").className = "link-glow";
    document.getElementById("flower-proof").className = "flower-glow";
  };
  //home
  document.getElementById("link-contact").onmouseover = function(){
    document.getElementById("link-contact").className = "link-glow";
    document.getElementById("flower-contact").className = "flower-glow";
  };
  document.getElementById("flower-contact").onmouseover = function(){
    document.getElementById("link-contact").className = "link-glow";
    document.getElementById("flower-contact").className = "flower-glow";
  };
    
  //register links and flowers for noglow effect
  //
  //home
  document.getElementById("link-home").onmouseout = function(){
    document.getElementById("link-home").className = "link-noglow";
    document.getElementById("flower-home").className = "flower-noglow";
  };
  document.getElementById("flower-home").onmouseout = function(){
    document.getElementById("link-home").className = "link-noglow";
    document.getElementById("flower-home").className = "flower-noglow";
  };
  //home
  document.getElementById("link-about").onmouseout = function(){
    document.getElementById("link-about").className = "link-noglow";
    document.getElementById("flower-about").className = "flower-noglow";
  };
  document.getElementById("flower-about").onmouseout = function(){
    document.getElementById("link-about").className = "link-noglow";
    document.getElementById("flower-about").className = "flower-noglow";
  };
  //home
  document.getElementById("link-proof").onmouseout = function(){
    document.getElementById("link-proof").className = "link-noglow";
    document.getElementById("flower-proof").className = "flower-noglow";
  };
  document.getElementById("flower-proof").onmouseout = function(){
    document.getElementById("link-proof").className = "link-noglow";
    document.getElementById("flower-proof").className = "flower-noglow";
  };
  //home
  document.getElementById("link-contact").onmouseout = function(){
    document.getElementById("link-contact").className = "link-noglow";
    document.getElementById("flower-contact").className = "flower-noglow";
  };
  document.getElementById("flower-contact").onmouseout = function(){
    document.getElementById("link-contact").className = "link-noglow";
    document.getElementById("flower-contact").className = "flower-noglow";
  };
    
  function getdata(url, page){
    $.ajax({
      type:"GET",
      url: url,
      dataType:"html",
      success:function(data){
        $("#content").html(data);
      },
      error:function(thrownError){
        console.log(thrownError);
      }
    });
  }
  getdata("./home.html");
});


